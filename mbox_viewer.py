import tkinter as tk
from tkinter import filedialog, ttk
import mailbox

class MboxViewer:

    def __init__(self, root):
        self.root = root
        self.root.title("Mbox Viewer")

        self.mbox = None

        # Search bar components
        self.search_label = tk.Label(root, text="Search:")
        self.search_label.pack(side=tk.LEFT, padx=5, pady=5)
        
        self.search_entry = tk.Entry(root)
        self.search_entry.pack(side=tk.LEFT, padx=5, pady=5)
        
        self.search_button = tk.Button(root, text="Search", command=self.search_emails)
        self.search_button.pack(side=tk.LEFT, padx=5, pady=5)

        # Treeview to display emails
        self.tree = ttk.Treeview(root, columns=("Subject", "From", "Date"))
        self.tree.heading("#1", text="Subject")
        self.tree.heading("#2", text="From")
        self.tree.heading("#3", text="Date")
        self.tree.bind("<Double-1>", self.open_email)
        self.tree.pack(pady=20, padx=20, fill=tk.BOTH, expand=True)

        menu = tk.Menu(root)
        root.config(menu=menu)

        file_menu = tk.Menu(menu)
        menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Open...", command=self.open_mbox)
        file_menu.add_command(label="Exit", command=root.quit)

    def open_mbox(self):
        filename = filedialog.askopenfilename()
        if not filename:
            return

        self.mbox = mailbox.mbox(filename)
        self.load_emails()

    def load_emails(self, search_term=None):
        self.tree.delete(*self.tree.get_children())
        
        for index, msg in enumerate(self.mbox):
            content = msg.as_string().lower()
            if search_term:
                if search_term.lower() not in content:
                    continue
            self.tree.insert("", "end", values=(msg["subject"], msg["from"], msg["date"]), iid=index)

    def search_emails(self):
        search_term = self.search_entry.get()
        self.load_emails(search_term)

    def open_email(self, event):
        item = self.tree.selection()[0]
        msg = self.mbox[int(item)]

        EmailWindow(self.root, msg)


class EmailWindow:

    def __init__(self, parent, msg):
        self.window = tk.Toplevel(parent)
        self.window.title(f"Email from {msg['from']}")

        text_widget = tk.Text(self.window, wrap=tk.WORD, height=30, width=80)
        text_widget.pack(padx=10, pady=10)

        payload = msg.get_payload(decode=True)
        if payload:
            text_widget.insert(tk.END, payload.decode(errors='replace'))
        else:
            text_widget.insert(tk.END, "[No message content available]")


if __name__ == "__main__":
    root = tk.Tk()
    viewer = MboxViewer(root)
    root.mainloop()

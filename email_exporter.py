import imaplib
import email
from mailbox import mbox
import datetime
import os

EMAIL = 'johndoe@example.com'
PASSWORD = 'password'
IMAP_SERVER = 'imap.example.com'

def fetch_emails(folder):
    print(f"Connecting to IMAP server {IMAP_SERVER}...")
    
    # Connect to the IMAP server
    mail = imaplib.IMAP4_SSL(IMAP_SERVER)
    mail.login(EMAIL, PASSWORD)
    
    print(f"Fetching emails from the '{folder}' folder...")
    
    # Select the desired folder
    mail.select(folder)

    # Search for all emails
    status, email_ids = mail.search(None, 'ALL')
    email_ids = email_ids[0].split()

    # Fetch the emails
    emails = []
    for e_id in email_ids:
        status, data = mail.fetch(e_id, '(RFC822)')
        raw_email = data[0][1]
        msg = email.message_from_bytes(raw_email)
        emails.append(msg)

    # Logout from the IMAP server
    mail.logout()
    
    print(f"Fetched {len(emails)} emails from the '{folder}' folder.")
    return emails

def save_to_mbox(emails, filename):
    print(f"Saving emails to {filename}...")
    
    # Create or open the mbox file and save emails to it
    box = mbox(filename)
    for msg in emails:
        box.add(msg)
    box.close()

    print(f"Saved emails to {filename}.")

# Get the current timestamp and create the directory name
timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
directory_name = f"{EMAIL}_{timestamp}"

print(f"Creating directory {directory_name}...")
os.makedirs(directory_name, exist_ok=True)

inbox_emails = fetch_emails('inbox')
save_to_mbox(inbox_emails, os.path.join(directory_name, 'inbox.mbox'))

sent_emails = fetch_emails('Sent')
save_to_mbox(sent_emails, os.path.join(directory_name, 'sent.mbox'))

print(f"Process complete! Saved {len(inbox_emails)} emails to {directory_name}/inbox.mbox and {len(sent_emails)} emails to {directory_name}/sent.mbox.")

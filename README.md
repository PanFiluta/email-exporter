# Email Exporter

A Python script to fetch and save emails from an IMAP server into an mbox file format.

## Getting started

To get started with the email exporter, follow the instructions below.

## Setup

1. Ensure you have Python installed on your machine.

2. Clone the repository:

```
git clone https://gitlab.com/PanFiluta/email-exporter.git
cd email-exporter
```

3. Update the `EMAIL`, `PASSWORD`, and `IMAP_SERVER` variables in the script with your credentials.

4. Run the script:

```
python3 email_exporter.py
```

This will fetch emails from your inbox and sent folders, then save them in mbox format in a directory named after your email and the current timestamp.

## Support

For issues or queries related to the email exporter, please raise an issue in the GitLab repository.

## Contributing

Contributions are welcome! Please create a merge request on GitLab for any enhancements or fixes.

## Authors and acknowledgment

Original script created by [Dominik Čampa](mailto:campadominik.dc@gmail.com).

## License

This project is open-source and available under the MIT License.

## Project status

This script is actively maintained. For any new features or issues, please refer to the GitLab repository.
